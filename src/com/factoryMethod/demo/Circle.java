package com.factoryMethod.demo;

public class Circle implements Shape{
    @Override
    public void draw() {
        System.out.println("Inside com.factoryMethod.demo.Circle::draw() method.");
    }
}
