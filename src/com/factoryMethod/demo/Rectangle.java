package com.factoryMethod.demo;

public class Rectangle implements Shape {
    @Override
    public void draw() {
        System.out.println("Inside com.factoryMethod.demo.Rectangle::draw() method.");
    }
}
