package com.factoryMethod.demo;

public interface Shape {
    void draw();
}
