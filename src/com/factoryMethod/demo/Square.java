package com.factoryMethod.demo;

public class Square implements Shape{
    @Override
    public void draw() {
        System.out.println("Inside com.factoryMethod.demo.Square::draw() method.");
    }
}
